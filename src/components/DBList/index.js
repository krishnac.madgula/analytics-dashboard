import React, { useEffect, useState, Fragment } from "react";
import Loader from "components/Loader";
import TableList from "components/TableList";

import "./style.css";

function DBList({ onClick, selectedDBTable }) {
  const [dbList, setDBList] = useState();

  useEffect(() => {
    // Fetch lsit of DBs
    // Todo: Replace with GET http://localhost:9170/information-schema/dbInfo
    fetch("https://api.myjson.com/bins/160xxp")
      .then(resp => resp.json())
      .then(resp => {
        setDBList(resp.data);
      });
  }, []);

  return (
    <div className="db-list-block">
      {!dbList ? (
        <Loader />
      ) : (
        <Fragment>
          <div className="db-list-column">
            {dbList.map(db => (
              <div
                className="db-name"
                onClick={() =>
                  onClick({ ...selectedDBTable, db, table: undefined })
                }
              >
                {db}
              </div>
            ))}
          </div>
          {selectedDBTable.db && (
            <TableList selectedDBTable={selectedDBTable} onClick={onClick} />
          )}
        </Fragment>
      )}
    </div>
  );
}

export default DBList;
