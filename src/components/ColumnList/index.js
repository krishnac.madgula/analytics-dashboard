import React, { useEffect, useState } from "react";
import Loader from "components/Loader";
import CardBody from "components/Card/CardBody.js";
import Table from "components/Table/Table.js";

function ColumnList() {
  const [columnList, setColumnList] = useState();

  useEffect(() => {
    fetch("https://api.myjson.com/bins/nnqrt")
      .then(resp => resp.json())
      .then(resp => {
        setColumnList(resp.data);
      });
  }, []);

  return (
    <div className="columns-block">
      {!columnList ? (
        <Loader />
      ) : (
        <CardBody>
          <Table
            tableHeaderColor="warning"
            tableHead={["ColumnName", "DataType"]}
            tableData={columnList.map(col => [col.ColumnName, col.DataType])}
          />
        </CardBody>
      )}
    </div>
  );
}

export default ColumnList;
