import React, { useEffect, useState } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Loader from "components/Loader";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  }
}));

const joinTypes = [
  { label: "Inner Join", value: "Inner Join" },
  { label: "Left Outer Join", value: "Left Outer Join" },
  { label: "Right Outer Join", value: "Right Outer Join" }
];

function ChartForm({ dataSources, onSubmit }) {
  const [columnList, setColumnList] = useState();
  const [values, setValues] = useState({});

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  useEffect(() => {
    fetch("https://api.myjson.com/bins/nnqrt")
      .then(resp => resp.json())
      .then(resp => {
        setColumnList(resp.data);
      });
  }, [values.first, values.second]);

  const classes = useStyles();
  const options = dataSources.map(ds => ({
    label: `${ds.db}/${ds.table}`,
    value: `${ds.db}/${ds.table}`
  }));
  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        onSubmit({ ...values });
      }}
    >
      <div className="chart-form-row">
        <TextField
          select
          className={classes.textField}
          label="Select 1st Datasource"
          value={values.first}
          onChange={handleChange("first")}
        >
          {options.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          select
          className={classes.textField}
          label="Select 2nd Datasource"
          value={values.second}
          onChange={handleChange("second")}
        >
          {options.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </div>
      <div className="chart-form-row">
        <TextField
          select
          label="Select Join Type"
          className={classes.textField}
          value={values.currency}
          onChange={handleChange("currency")}
        >
          {joinTypes.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </div>
      <div className="chart-form-row">
        <TextField
          label="Enter the condition query"
          className={classes.textField}
          value={values.query}
          onChange={handleChange("query")}
          multiline
          rows={4}
        />
      </div>
      <div className="chart-form-row">
        <h4>Select the colums to display</h4>
        <FormControlLabel
          control={
            <Checkbox
              checked={values.checkedA}
              onChange={handleChange("all")}
              value="all"
            />
          }
          label="All"
        />
        <h5>From Datasource 1</h5>
        {!columnList ? (
          <Loader />
        ) : (
          columnList.map(col => (
            <FormControlLabel
              control={
                <Checkbox
                  checked={values[col.ColumnName]}
                  onChange={handleChange(col.ColumnName)}
                  value={col.ColumnName}
                />
              }
              label={col.ColumnName}
              key={col.ColumnName}
            />
          ))
        )}
        <h5>From DataSorce 2</h5>
        {!columnList ? (
          <Loader />
        ) : (
          columnList.map(col => (
            <FormControlLabel
              control={
                <Checkbox
                  checked={values[col.ColumnName]}
                  onChange={handleChange(col.ColumnName)}
                  value={col.ColumnName}
                />
              }
              label={col.ColumnName}
              key={col.ColumnName}
            />
          ))
        )}
      </div>
      <Button
        variant="contained"
        color="primary"
        className="add-ds-btn"
        onClick={e => {
          e.preventDefault();
          onSubmit({ ...values });
          setValues({});
        }}
      >
        Submit
      </Button>
    </form>
  );
}

export default ChartForm;
