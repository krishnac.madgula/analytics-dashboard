import React, { Component } from "react";

import "./style.css";

class Popup extends Component {
  constructor() {
    super();
    this.checkESC = this.checkESC.bind(this);
    this.checkClick = this.checkClick.bind(this);
  }
  checkESC(event) {
    // Check if ESC key is pressed
    if (event.which === 27) {
      this.props.onClose();
    }
  }
  checkClick(event) {
    event.stopPropagation();
    // Check if the target is modal-backdrop
    if (this.modalRef === event.target) {
      this.props.onClose();
    }
  }

  componentDidMount() {
    if (this.props.show) {
      this.modalRef && this.modalRef.focus();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.show !== prevProps.show) {
      this.modalRef && this.modalRef.focus();
    }
  }

  render() {
    let { props } = this;
    return (
      <div
        className={("popup " + (props.className || "")).trim()}
        ref={node => {
          this.modalRef = node;
        }}
        onKeyDown={this.checkESC}
        onClick={this.checkClick}
        tabIndex="0"
      >
        <div className={("popup-child " + (props.childClass || "")).trim()}>
          {props.children}
        </div>
      </div>
    );
  }
}

export default Popup;
