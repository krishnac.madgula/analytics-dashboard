import React from 'react'

import './style.css'

const Loader = (props) => (
  <div className='loader' />
)

export default Loader
