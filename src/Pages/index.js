import React, { useState, Fragment } from "react";
import Popup from "../components/Popup";
import DBList from "../components/DBList";
import ChartThumbnail from "../components/ChartThumbnail/ChartThumbnail";
import Button from "@material-ui/core/Button";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import GridContainer from "components/Grid/GridContainer.js";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import ChartAddForm from "components/ChartAddForm";
import "./index.css";

import dbImg from "../images/database.svg";
import plusImg from "../images/plus.png";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  }
}));

const data = {
  labels: ["M", "T", "W", "T", "F", "S", "S"],
  series: [[12, 17, 7, 17, 23, 18, 38]]
};

function HomePage() {
  const [showModal, setShowModal] = useState(false);
  const [selectedDBTable, handleSetDBTable] = useState({});
  const [addedDSList, setAddedDSList] = useState([]);
  const [chartConfigs, setChartConfigs] = useState([]);
  const [showChartPopup, setShowChartPopup] = useState(false);

  const classes = useStyles();

  const handleAddDS = e => {
    e && e.preventDefault();
    setShowModal(!showModal);
    if (selectedDBTable && selectedDBTable.db && selectedDBTable.table) {
      setAddedDSList([...addedDSList, selectedDBTable]);
      handleSetDBTable({});
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
    handleSetDBTable({});
  };

  const handleAddChart = () => {
    setShowChartPopup(!showChartPopup);
  };

  const handleSubmitAddChart = chartDetails => {
    handleAddChart();
    setChartConfigs([...chartConfigs, chartDetails]);
  };

  return (
    <div className="home-page">
      <div className="main-db-block">
        <div className="db-add-btn" onClick={handleAddDS}>
          <img src={dbImg} alt="add btn" />
          <img src={plusImg} alt="add btn" />
        </div>
        {showModal && (
          <Popup onClose={handleCloseModal} childClass="db-popup">
            <DBList
              onClick={handleSetDBTable}
              selectedDBTable={selectedDBTable}
            />
            {selectedDBTable.db && selectedDBTable.table && (
              <Button
                variant="contained"
                color="primary"
                className="add-ds-btn"
                onClick={handleAddDS}
              >
                Add data source
              </Button>
            )}
          </Popup>
        )}
        {addedDSList && addedDSList.length > 0 && (
          <Fragment>
            <h2>Datasource pool</h2>
            <div className="ds-list">
              <GridContainer>
                {addedDSList.map(ds => (
                  <GridItem key={`${ds.db}/${ds.table}`} xs={10} sm={10} md={6}>
                    <div className="ds-unit">
                      <img
                        src={dbImg}
                        alt={`${ds.db}/${ds.table}`}
                        className="ds-unit-icon"
                      />
                      <div>{`${ds.db}/${ds.table}`}</div>
                    </div>
                  </GridItem>
                ))}
              </GridContainer>
            </div>
          </Fragment>
        )}
        <Divider variant="middle" />
        {showChartPopup && (
          <Popup childClass="add-chart-popup" onClose={handleAddChart}>
            <ChartAddForm onSubmit={handleSubmitAddChart} />
          </Popup>
        )}
      </div>
      {Boolean(chartConfigs.length) && (
        <Fragment>
          <h2>Charts</h2>
          <GridContainer>
            {chartConfigs.map(chartDetails => (
              <GridItem key={chartDetails.name}>
                <Card chart>
                  <CardHeader color="success">
                    <ChartThumbnail
                      data={data}
                      type="Bar"
                      dataSources={addedDSList}
                    />
                  </CardHeader>
                  <h4>{chartDetails.name}</h4>
                </Card>
              </GridItem>
            ))}
          </GridContainer>
        </Fragment>
      )}
      <Button
        disabled={!(addedDSList && addedDSList.length)}
        onClick={handleAddChart}
        variant="contained"
        color="primary"
        className={classes.button}
      >
        Add Chart
      </Button>
    </div>
  );
}

export default HomePage;
